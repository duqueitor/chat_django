$(document).ready(function(){

	$('#enviar').click(function(){
		
		
		mensaje      = $("#mensaje").val()
		autor        = $("#autor").val()
		destinatario = $("#destinatario").val()
		conver_id    = $("#conver_id").val()

		console.log('mensaje: ', mensaje)
		console.log('autor: ', autor)
		console.log('destinatario: ', destinatario)



		var form_data = new FormData();
	    form_data.append("mensaje"       , mensaje);
	    form_data.append("autor"         , autor);
	    form_data.append("destinatario"  , destinatario);
	    form_data.append("conver_id"     , conver_id);

		$.ajax({
        
	        url         : '/guardar_mensaje',
	        dataType    : 'text',
	        type        : 'POST',
	        cache       : false,
	        contentType : false,
	        processData : false,
	        data        : form_data,
	        
	        success: function(response) {
	            console.log("RESPUESTA: ", response);
	            var text = response;
	           	
	           	$('#mensaje_nuevo').show()
	            $('#mensaje_nuevo').html(text)
	            $('#mensaje').val('')
	        },

	        error: function(jqXHR,estado,error) {
	            console.log("estado",estado);
	            console.log(error);
	        },
	    });   

	});

	$('#limpiar_conver').click(function(){
		
		conver_id    = $("#conver_id").val()

		var form_data = new FormData();
	    form_data.append("conver_id" , conver_id);

		$.ajax({
        
	        url         : '/limpiar_conver',
	        dataType    : 'text',
	        type        : 'POST',
	        cache       : false,
	        contentType : false,
	        processData : false,
	        data        : form_data,
	        
	        success: function(response) {
	            console.log("RESPUESTA: ", response);
	            var text = response;
	            $('#mensaje').val('')
	            $('#mensajes').html('</br></br>')
	        },

	        error: function(jqXHR,estado,error) {
	            console.log("estado",estado);
	            console.log(error);
	        },
	    });   

	});

});
