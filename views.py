#ADRIAN DUQUE ANGUERA
#TEL: 689263795

from django.shortcuts               import render, render_to_response, redirect
from django.views.decorators.csrf   import csrf_exempt
from django.http                    import HttpResponseRedirect, HttpResponse
from django.template                import Template , Context
from django.template.response       import TemplateResponse
from django.contrib.auth.models     import User
from django.contrib.auth.forms      import UserCreationForm, AuthenticationForm
from django.contrib.auth            import authenticate
from django.contrib                 import auth
from django.core.urlresolvers       import reverse

from .models import Conver, Mensajes

import os
import itertools
import urllib
import sys

def users(request):

	users   = User.objects.all().exclude(username = 'admin')
	context = {'users' : users}
	
	return render(request, 'chat/index.html', context)

@csrf_exempt
def perfil(request, user):
	print('perfil de ' + user)

	try:
		User.objects.get(username = user)
		
		if request.method == 'POST':
			print('eliminar conversacion')

			id_conver = request.POST.get('id_conver')

			Mensajes.objects.filter(conver__id_conver = id_conver).delete()
			Conver.objects.get(id_conver = id_conver).delete()

			print('conversacion eliminada')

		conversaciones_abiertas = Conver.objects.filter(user1__username = user) | Conver.objects.filter(user2__username = user)
		conversaciones = []
		dic = {}

		if conversaciones_abiertas == '[]':
			print ('Ninguna conver abierta')
		else:
			for i in conversaciones_abiertas:
				m1 = i.user1
				m2 = i.user2
				if m1.username == user:
					colega = m2
				else:
					colega = m1

				print ('Conversacion abierta con: ' + colega.username)
				dic['conversacion'] = i.id_conver
				dic['destinatario'] = colega.username

				conversaciones.append(dic)
				dic= {}

		mis_contactos   = Conver.objects.filter(user1__username = user) | Conver.objects.filter(user2__username = user)
		con_los_q_hablo = []
		
		for i in mis_contactos:
			u1 = i.user1.username
			u2 = i.user2.username

			if(u1 == user):
				con_los_q_hablo.append(u2)
			else:
				con_los_q_hablo.append(u1)

		todos_usuarios = User.objects.all().exclude(username = 'admin') & User.objects.all().exclude(username = user)
		lista = []
		for i in todos_usuarios:
			lista.append(i.username)

		usuarios_no_hablo = list(set(lista) - set(con_los_q_hablo))
		print('ABRIR CONVERSACION CON: ' + str(usuarios_no_hablo))

		context = { 'user' : user,
		  'conversaciones' : conversaciones,
		  'no_hablo'       : usuarios_no_hablo,
		}
		
		return render(request, 'chat/perfil.html', context)
	
	except User.DoesNotExist:
		context = {'error' : 'No existe el usuario ' + user + ' .'}
		return render(request, 'chat/no_exists.html', context)
	
def conver(request, user, conver):
	#comprobar si es la primera vez que hablan

	print('conver: ' + conver)
	try:
		conver_id = Conver.objects.get(id_conver = conver)

		u1 = conver_id.user1.username
		u2 = conver_id.user2.username

		if u1 == user:
			destinatario = u2
		else:
			destinatario = u1

		mensajes = Mensajes.objects.filter(conver = conver)

		context = {'destinatario' : destinatario,
					'mensajes'    : mensajes,
					'conver_id'   : conver_id.id_conver,
					'autor'       : user
				}

		return render(request, 'chat/conver.html', context)
	
	except Conver.DoesNotExist:
		context = {'error' : 'La conversacion con id ' + conver + ' no existe.'}
		return render(request, 'chat/no_exists.html', context)

def crear_conver(request, user, destinatario):
	#last = Conver.objects.latest('id_conver')
	
	conver_new = Conver()
	
	conver_new.user1 = User.objects.get(username = user)
	conver_new.user2 = User.objects.get(username = destinatario)
	conver_new.save()

	url = '/' + user
	return redirect(url)