from django.contrib.auth.models import User
from django.db                  import models
from datetime                   import date, datetime, timedelta

class Conver(models.Model):

	id_conver = models.AutoField(primary_key=True)
	user1     = models.ForeignKey(User, related_name='user1')
	user2     = models.ForeignKey(User, related_name='user2')
	
	def __str__(self):
		return str(self.id_conver)

class Mensajes(models.Model):
	conver       = models.ForeignKey(Conver)
	autor        = models.ForeignKey(User, related_name='autor')
	destinatario = models.ForeignKey(User, related_name='destinatario')
	mensaje      = models.CharField(max_length=500, blank=True)
	fecha        = models.DateTimeField(blank=True, default = datetime.now())

	def __str__(self):
		return str(self.autor.username)