#!/usr/bin/python
from django.shortcuts               import render, render_to_response, redirect
from django.views.decorators.csrf   import csrf_exempt
from django.http                    import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models     import User

from datetime import datetime, date, time, timedelta 

from .models import Mensajes, Conver


@csrf_exempt
def guardar_mensaje(request):
	
	print('Mensaje')

	conver_id = request.POST.get('conver_id')
	conver_id = conver_id.strip()

	mensaje = request.POST.get('mensaje')
	mensaje = mensaje.strip()

	autor = request.POST.get('autor')
	autor = autor.strip()

	destinatario = request.POST.get('destinatario')
	destinatario = destinatario.strip()


	conver       = Conver.objects.get(id_conver = conver_id)
	autor        = User.objects.get(username = autor)
	destinatario = User.objects.get(username = destinatario)

	mensaje_new = Mensajes()

	mensaje_new.conver       = conver
	mensaje_new.mensaje      = mensaje
	mensaje_new.autor        = autor
	mensaje_new.destinatario = destinatario

	mensaje_new.save()

	return HttpResponse(mensaje)

@csrf_exempt
def limpiar_conver(request):
	conver_id = request.POST.get('conver_id')
	conver_id = conver_id.strip()

	Mensajes.objects.filter(conver__id_conver = conver_id).delete()


	return HttpResponse('correcto')
