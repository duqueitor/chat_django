from django.conf.urls import patterns, include, url
from django.contrib   import admin
admin.autodiscover()

from . import views
from . import ajax

urlpatterns = [
	url(r'^$'                       , views.users        , name="index"  ),
	url(r'^(.+)/$'                  , views.perfil       , name="perfil" ),
	url(r'^(.+)/(.+)/crear_conver$' , views.crear_conver , name="crear_conver" ),
	url(r'^(.+)/(.+)$'              , views.conver       , name="conver" ),


	url(r'^guardar_mensaje$' , ajax.guardar_mensaje),
	url(r'^limpiar_conver$'  , ajax.limpiar_conver),
]
